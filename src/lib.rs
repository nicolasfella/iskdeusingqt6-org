/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

pub mod ci {

    pub mod kde {

        use serde::Deserialize;
        use serde_yaml::Value;
        use std::collections::BTreeMap as Map;

        #[derive(Deserialize, Default)]
        pub struct KdeCi {
            #[serde(rename = "Dependencies")]
            pub dependencies: Option<Vec<Dependencies>>,
            #[serde(rename = "Options")]
            pub options: Option<Options>,
        }

        #[derive(Deserialize)]
        pub struct Options {
            #[serde(rename = "require-passing-tests-on")]
            pub require_passing_tests_on: Option<Vec<String>>,
        }

        #[derive(Deserialize)]
        pub struct Dependencies {
            pub require: Require,
        }

        #[derive(Deserialize)]
        pub struct Require {
            #[serde(flatten)]
            pub other: Map<String, Value>,
        }
    }

    pub mod gitlab {
        use serde::Deserialize;

        #[derive(Deserialize, Default)]
        pub struct GitlabCi {
            pub include: Vec<Thing>,
        }

        #[derive(Deserialize)]
        pub struct Thing {
            pub project: String,
            pub file: Vec<String>,
        }
    }
}

pub mod project {

    use crate::ci::gitlab::GitlabCi;
    use crate::ci::kde::KdeCi;
    use crate::util;
    use std::env;

    #[derive(Debug, PartialEq)]
    pub enum Platform {
        LinuxQt5,
        LinuxQt6,
        FreeBSDQt5,
        FreeBSDQt6,
        AndroidQt5,
        AndroidQt6,
        WindowsQt5,
        WindowsQt6,
    }

    #[derive(PartialEq, Eq, Debug)]
    pub struct Project {
        base_name: String,
    }

    impl Project {
        pub fn from_full_name(full_name: String) -> Self {
            Project {
                base_name: full_name.split("/").into_iter().nth(1).unwrap().to_string(),
            }
        }

        pub fn from_base_name(base_name: String) -> Self {
            Project { base_name }
        }

        pub fn base_name(&self) -> String {
            self.base_name.to_string()
        }

        pub fn printable_name(&self) -> String {
            self.base_name().replace("-", "_")
        }

        pub fn display_name(&self) -> String {
            self.base_name.to_string()
        }

        pub fn kde_ci(&self) -> Option<KdeCi> {
            let projects_home = env::var("KQT6_PROJECT_HOME").unwrap();

            let maybe_kdeci_file = std::fs::File::open(format!(
                "{}/{}/.kde-ci.yml",
                projects_home,
                self.base_name()
            ));

            if maybe_kdeci_file.is_err() {
                return None;
            }

            return serde_yaml::from_reader(maybe_kdeci_file.unwrap()).unwrap();
        }

        pub fn gitlab_ci(&self) -> Option<GitlabCi> {
            let projects_home = env::var("KQT6_PROJECT_HOME").unwrap();

            let maybe_gitlabci_file = std::fs::File::open(format!(
                "{}/{}/.gitlab-ci.yml",
                projects_home,
                self.base_name()
            ));

            if maybe_gitlabci_file.is_err() {
                return None;
            }

            return serde_yaml::from_reader(maybe_gitlabci_file.unwrap()).ok();
        }

        pub fn has_ci(&self) -> bool {
            self.gitlab_ci().is_some()
        }

        pub fn has_qt6(&self) -> bool {
            let foo = self.gitlab_ci().unwrap_or_default().include;

            if foo.is_empty() {
                println!("nope {}", self.base_name);
                return false;
            }

            return foo
                .first()
                .unwrap()
                .file
                .iter()
                .any(|job| job.contains("linux-qt6"));
        }

        pub fn ci_platforms(&self) -> Vec<Platform> {
            self.gitlab_ci().unwrap_or_default().include.first().unwrap().file.iter().filter_map(|line| match line.as_str() {
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/linux.yml" => Some(Platform::LinuxQt5),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/linux-qt6.yml" => Some(Platform::LinuxQt6),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/freebsd.yml" => Some(Platform::FreeBSDQt5),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/freebsd-qt6.yml" => Some(Platform::FreeBSDQt6),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/android.yml" => Some(Platform::AndroidQt5),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/android-qt6.yml" => Some(Platform::AndroidQt6),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/windows.yml" => Some(Platform::WindowsQt5),
                "https://invent.kde.org/sysadmin/ci-utilities/raw/master/gitlab-templates/windows-qt6.yml" => Some(Platform::WindowsQt6),
                _ => None,
            }).collect()
        }
    }

    pub fn get_projects(file: &str) -> Vec<Project> {
        let mut result = vec![];

        if let Ok(lines) = util::read_lines(file) {
            for line in lines {
                if let Ok(project_name) = line {
                    let ignored = vec![
                        "education/kdeedu-data",
                        "sdk/releaseme",
                        "sdk/kde-ruleset",
                        "sdk/kde-dev-utils",
                        "sdk/kde-dev-scripts",
                        "sdk/inqlude-client",
                        "sdk/git-lab",
                        "sdk/clazy",
                        "plasma/oxygen-gtk",
                        "plasma/oxygen-sounds",
                        "plasma-mobile/plasma-phone-settings",
                        "plasma/breeze-grub",
                        "libraries/kross-interpreters",
                        "sdk/kdesrc-build",
                        "sdk/licensedigger",
                        "sdk/doxyqml",
                        "plasma/kwayland-integration",
                        "graphics/libkipi",
                        "graphics/kipi-plugins",
                        "libraries/phonon-vlc",
                    ];

                    let found = ignored.iter().find(|thing| **thing == project_name);

                    if found.is_some() {
                        continue;
                    }

                    result.push(Project::from_full_name(project_name));
                }
            }
        }

        return result;
    }
}

pub mod util {

    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    // The output is wrapped in a Result to allow matching on errors
    // Returns an Iterator to the Reader of the lines of the file.
    pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}
