/*
    SPDX-FileCopyrightText: 2022 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: MIT
*/

use kqt6::project;
use kqt6::project::Platform;
use serde::Serialize;
use std::env;
use std::fs::File;
use std::io::Write;
use tinytemplate::TinyTemplate;

#[derive(Serialize)]
struct Context {
    missing_freebsd: Vec<String>,
    missing_android: Vec<String>,
    missing_windows: Vec<String>,
}

fn main() {
    let all_projects = project::get_projects(&env::var("KQT6_PROJECTS_FILE").unwrap());

    let projects_with_qt6: Vec<&project::Project> = all_projects
        .iter()
        .filter(|project| project.has_qt6())
        .collect();

    let mut missing_freebsd: Vec<&project::Project> = vec![];
    let mut missing_android: Vec<&project::Project> = vec![];
    let mut missing_windows: Vec<&project::Project> = vec![];

    for project in projects_with_qt6 {
        let platforms = project.ci_platforms();

        if platforms.contains(&Platform::FreeBSDQt5) && !platforms.contains(&Platform::FreeBSDQt6) {
            missing_freebsd.push(project);
        }

        if platforms.contains(&Platform::AndroidQt5) && !platforms.contains(&Platform::AndroidQt6) {
            missing_android.push(project);
        }

        if platforms.contains(&Platform::WindowsQt5) && !platforms.contains(&Platform::WindowsQt6) {
            missing_windows.push(project);
        }
    }

    let template_file: String = env::var("KQT6_PLATFORMS_TEMPLATE").unwrap();

    let template: String = std::fs::read_to_string(template_file).unwrap();

    let mut tt = TinyTemplate::new();
    tt.add_template("the_template", &template).unwrap();

    let context = Context {
        missing_freebsd: missing_freebsd
            .iter()
            .map(|project| project.base_name())
            .collect(),
        missing_android: missing_android
            .iter()
            .map(|project| project.base_name())
            .collect(),
        missing_windows: missing_windows
            .iter()
            .map(|project| project.base_name())
            .collect(),
    };

    let rendered = tt.render("the_template", &context);

    let ouput_file: String = env::var("KQT6_PLATFORMS_OUTPUT").unwrap();

    let mut output_file = File::create(ouput_file).unwrap();

    output_file.write_all(rendered.unwrap().as_bytes()).unwrap();
}
